﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_II__Tarea_4_
{
    class Program
    {
        static void Main(string[] args)
        {
            int limitRen = 7;
            int limiteCol = 3;
            int cantidad_sucursales = 7;
            string[] barrios = new string[7];
            barrios[0] = "Once";
            barrios[1] = "San Nicolas";
            barrios[2] = "Almagro";
            barrios[3] = "Microcentro";
            barrios[4] = "Palermo";
            barrios[5] = "San Cristobal";
            barrios[6] = "Parque Patricios";
            int[,] dato_sucur = new int[limitRen, limiteCol];

            Sucursales[] objSucursal = new Sucursales[cantidad_sucursales];
            Sucursales datosSucursales = new Sucursales();

            for (int i = 0; i < 7; i++) objSucursal[i] = new Sucursales();


            for (int i = 0; i < 7; i++)
            {
                int num_sucu;
                int cant_clientes;
                int cant_empleados;
                int cant_ventas;
                Console.WriteLine("Ingrese un numero de la sucursal que sea menor o igual a 7:");
                num_sucu = Convert.ToInt32(Console.ReadLine());
                while (num_sucu > 7 || num_sucu <= 0)
                {
                    Console.WriteLine("Numero Invalido, volver a ingresar otro:");
                    num_sucu = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine("Ingrese un monto de ventas:");
                dato_sucur[i, 0] = Convert.ToInt32(Console.ReadLine());
                cant_ventas = dato_sucur[i, 0];

                Console.WriteLine("Ingrese cantidad de empleados:");
                dato_sucur[i, 1] = Convert.ToInt32(Console.ReadLine());
                cant_empleados = dato_sucur[i, 1];

                Console.WriteLine("Ingrese cantidad de clientes por mes");
                dato_sucur[i, 2] = Convert.ToInt32(Console.ReadLine());
                cant_clientes = dato_sucur[i, 2];

                objSucursal[i].cargarDatos(num_sucu, cant_ventas, cant_empleados, cant_clientes);
                datosSucursales.maximaVentas(dato_sucur[i, 0], barrios[num_sucu - 1]);
                datosSucursales.promedioVentas(dato_sucur[i, 0], i);
                datosSucursales.promedioEmpleados(dato_sucur[i, 1], i);
                datosSucursales.ventasTotales(dato_sucur[i, 0], i);
                datosSucursales.minimaVentas(dato_sucur[i, 0], barrios[num_sucu - 1], i);
            }

            Console.WriteLine("\nHa listado del personal cargado");

            for (int i = 0; i < 7; i++)
            {
                objSucursal[i].mostrarDatos(barrios);
            }
            Console.WriteLine("Datos e General:");
            datosSucursales.mostrarInformacion();
            Console.ReadLine();
        }
    }
    public class Sucursales
    {

        //Atributos
        int numero_sucursal;
        int cant_ventas;
        int cant_clientes;
        int cant_empleados;
        int ventas_max = 0;
        int ventas_min = 0;
        string sucursal_max;
        string sucursal_min;
        int venta_promedio = 0;
        int ventas_total = 0;
        int empleados_promedio = 0;
        //Metodos

        public void ventasTotales(int all_ventas, int cant)
        {
            ventas_total += all_ventas;
        }

        public void minimaVentas(int ventas_general, string id_sucursal, int cant)
        {
            if (cant == 0)
            {
                sucursal_min = id_sucursal;
                ventas_min = ventas_general;
            }
            else
            {
                if (ventas_min > ventas_general)
                {
                    sucursal_min = id_sucursal;
                    ventas_min = ventas_general;
                }
            }
        }

        public void maximaVentas(int ventas_general, string id_sucursal)
        {
            if (ventas_max < ventas_general)
            {
                sucursal_max = id_sucursal;
                ventas_max = ventas_general;
            }
        }

        public void promedioVentas(int ventas, int cant)
        {
            venta_promedio += ventas;
            if (cant == 6)
            {
                venta_promedio = venta_promedio / 7;
            }
        }

        public void promedioEmpleados(int empleados, int cant)
        {
            empleados_promedio += empleados;
            if (cant == 6)
            {
                empleados_promedio = empleados_promedio / 7;
            }
        }

        public void mostrarInformacion()
        {
            Console.WriteLine("El promedio de ventas de las sucursale es de {0}", venta_promedio);
            Console.WriteLine("El promedio de empleados de las sucursales es de {0}", empleados_promedio);
            Console.WriteLine("El total de ventas de todas las Sucursales es de {0}", ventas_total);
            Console.WriteLine("La sucursal con mayor monto de ventas de {0} ,es la sucursal de {1}", ventas_max, sucursal_max);
            Console.WriteLine("La sucursal con menor monto de ventas de {0} ,es la sucursal de {1}", ventas_min, sucursal_min);

        }

        public void mostrarDatos(string[] barri_sucur)
        {
            Console.WriteLine("Sucursal {0} del barrio de {1}:", numero_sucursal, barri_sucur[numero_sucursal - 1]);
            Console.WriteLine("Monto de Ventas: {0}", cant_ventas);
            Console.WriteLine("Cantidad de empleados: {0}", cant_empleados);
            Console.WriteLine("Cantidad de clientes por mes: {0}", cant_clientes);
            Console.WriteLine("");

        }

        public void cargarDatos(int Numero_sucursal, int Cant_Ventas, int Cant_Empleados, int Cant_Clientes)
        {
            numero_sucursal = Numero_sucursal;
            cant_empleados = Cant_Empleados;
            cant_clientes = Cant_Clientes;
            cant_ventas = Cant_Ventas;
        }
    }
}
